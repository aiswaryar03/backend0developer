package com.dev.app;


import static spark.Spark.get;

import com.dev.external.UserInfo;
import com.dev.util.JsonUtil;

public class Main {
    public static void main(String[] args) {
        System.out.println("Server started...");
       //sample request http://localhost:4567/api/customer/0115465463
       
       get("api/customer/:number", (req, res) -> 
       {
    	   String key = req.headers("Authorization");
    	   System.out.println(key);
    	   res.type("application/json");
    	   return UserInfo.getInstance().getStandardResponse(req.params(":number"));  
       },JsonUtil.json());
    }
}
