package com.dev.util;

import com.google.gson.Gson; 
import spark.ResponseTransformer; 
/*
 * Implementation of a Json renderer through google GSON utility. 
 */ 
/*public class JsonTransformer implements ResponseTransformer { 
 
   private Gson gson = new Gson(); 
 
   @Override 
   public String render(Object model) { 
       return gson.toJson(model); 
   } 
 
}*/

public class JsonUtil {
	 
public static String toJson(Object object) {
 return new Gson().toJson(object);
}

public static ResponseTransformer json() {
 return JsonUtil::toJson;
}
}