package com.dev.external;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.dev.adaptor.Customer;
import com.dev.util.IConstant;
import com.fasterxml.jackson.databind.ObjectMapper;

public class UserInfo {
	private static final Logger LOGGER = Logger.getLogger(UserInfo.class);
	private static UserInfo self;

	private UserInfo() {

	}

	public static UserInfo getInstance() {
		if (self == null) {
			self = new UserInfo();
		}
		return self;
	}

	public JSONObject getStandardResponse(String mobileNo) {
		JSONObject main = new JSONObject();
		JSONObject address = new JSONObject();
		JSONObject additionalInfo = new JSONObject();
		JSONObject cus = new JSONObject();
		Customer customer = fetchData(mobileNo);
		cus.put("Id", customer.getCrmCustomerID());
		cus.put("Name", customer.getCustomerName());
		cus.put("nationality", customer.getNationality());
		cus.put("race", customer.getRace());
		cus.put("gender", customer.getGender());
		cus.put("IDNo", customer.getCustomerIDNo());
		cus.put("dateOfBirth", customer.getDateOfBirth());
		cus.put("number", customer.getCallerNumber());
		cus.put("customerId", customer.getCustomerIDNo());
		cus.put("customerIdType", customer.getCustomerIDType());

		address.put("buildingName", customer.getBuildingName());
		address.put("floorNumber", customer.getFloorNumber());
		address.put("streetType", customer.getStreetType());
		address.put("houseUnitLotNumber", customer.getHouseUnitLotNumber());
		address.put("postcode", customer.getPostcode());
		address.put("poBox", customer.getPoBox());
		address.put("state", customer.getState());
		address.put("city", customer.getCity());
		address.put("addressType", customer.getAddressType());
		cus.put("address", address);

		additionalInfo.put("creditLimit", customer.getCreditLimit());
		additionalInfo.put("bestCallTime", customer.getBestCallTime());
		additionalInfo.put("firstCallActivationDate", customer.getFirstCallActivationDate());
		additionalInfo.put("serviceType", customer.getServiceType());
		additionalInfo.put("product", customer.getProduct());
		additionalInfo.put("crmServiceID", customer.getCrmServiceID());
		additionalInfo.put("ivrLanguage", customer.getIvrLanguage());
		additionalInfo.put("serviceType", customer.getServiceType());
		additionalInfo.put("vipCode", customer.getVipCode());
		additionalInfo.put("segmentGroup", customer.getSegmentGroup());
		additionalInfo.put("pla", customer.getPlan());
		additionalInfo.put("creditLimit", customer.getCreditLimit());
		additionalInfo.put("serviceBundelClass", customer.getServiceBundleClass());
		
		cus.put("additionalInfo", additionalInfo);

		cus.put("resourceURL", "");

		main.put("Customer", cus);

		return main;

	}

	public Customer fetchData(String mobileNo) {
		Customer customer = null;
		try {
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
			Map<String, String> map = new HashMap<String, String>();
			map.put("Content-Type", "application/json");
			headers.setAll(map);

			ResponseEntity<String> response = new RestTemplate().getForEntity(IConstant.UserInfoUrl + mobileNo,
					String.class);
			ObjectMapper mapper = new ObjectMapper();
			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
			customer = mapper.readValue(jsonObject.get("outputSDResp").toString(), Customer.class);
			System.out.println("Response : " + customer);
		} catch (Exception e) {
			LOGGER.error("Error : ", e);
		}
		return customer;
	}

}
