package com.dev.adaptor;

public class Customer {
    private String crmServiceID;
    private String ivrLanguage;
    private String serviceType;
    private String product;
    private String crmCustomerID;
    private String customerName;
    private String accountCategory;
    private String dateOfBirth;
    private String nationality;
    private String race;
    private String gender;
    private String customerIDNo;
    private String customerIDType;
    private String vipCode;
    private String segmentGroup;
    private String segmentCode;
    private String segmentSubGroup;
    private String buildingName;
    private String floorNumber;
    private String streetType;
    private String streetAddress;
    private String houseUnitLotNumber;
    private String postcode;
    private String poBox;
    private String state;
    private String city;
    private String addressType;
    private String bestCallTime;
    private String tPIN;
    private String ivrHotlineExclusion;
    private String ivrVIPHotlineExclusion;
    private Long callerNumber;
    private String assetStatus;
    private String assetSubStatus;
    private String firstCallActivationDate;
    private String starterPackBrand;
    private String starterPackName;
    private String plan;
    private String registrationDate;
    private String ccToken;
    private String eliteIndicator;
    private String serviceBundleClass;
    private String creditLimit;
    private String errorCode;
    private String errorMessage;

    public String getCrmServiceID() {
        return crmServiceID;
    }

    public void setCrmServiceID(String crmServiceID) {
        this.crmServiceID = crmServiceID;
    }

    public String getIvrLanguage() {
        return ivrLanguage;
    }

    public void setIvrLanguage(String ivrLanguage) {
        this.ivrLanguage = ivrLanguage;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCrmCustomerID() {
        return crmCustomerID;
    }

    public void setCrmCustomerID(String crmCustomerID) {
        this.crmCustomerID = crmCustomerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAccountCategory() {
        return accountCategory;
    }

    public void setAccountCategory(String accountCategory) {
        this.accountCategory = accountCategory;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCustomerIDNo() {
        return customerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        this.customerIDNo = customerIDNo;
    }

    public String getCustomerIDType() {
        return customerIDType;
    }

    public void setCustomerIDType(String customerIDType) {
        this.customerIDType = customerIDType;
    }

    public String getVipCode() {
        return vipCode;
    }

    public void setVipCode(String vipCode) {
        this.vipCode = vipCode;
    }

    public String getSegmentGroup() {
        return segmentGroup;
    }

    public void setSegmentGroup(String segmentGroup) {
        this.segmentGroup = segmentGroup;
    }

    public String getSegmentCode() {
        return segmentCode;
    }

    public void setSegmentCode(String segmentCode) {
        this.segmentCode = segmentCode;
    }

    public String getSegmentSubGroup() {
        return segmentSubGroup;
    }

    public void setSegmentSubGroup(String segmentSubGroup) {
        this.segmentSubGroup = segmentSubGroup;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getStreetType() {
        return streetType;
    }

    public void setStreetType(String streetType) {
        this.streetType = streetType;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getHouseUnitLotNumber() {
        return houseUnitLotNumber;
    }

    public void setHouseUnitLotNumber(String houseUnitLotNumber) {
        this.houseUnitLotNumber = houseUnitLotNumber;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getBestCallTime() {
        return bestCallTime;
    }

    public void setBestCallTime(String bestCallTime) {
        this.bestCallTime = bestCallTime;
    }

    public String gettPIN() {
        return tPIN;
    }

    public void settPIN(String tPIN) {
        this.tPIN = tPIN;
    }

    public String getIvrHotlineExclusion() {
        return ivrHotlineExclusion;
    }

    public void setIvrHotlineExclusion(String ivrHotlineExclusion) {
        this.ivrHotlineExclusion = ivrHotlineExclusion;
    }

    public String getIvrVIPHotlineExclusion() {
        return ivrVIPHotlineExclusion;
    }

    public void setIvrVIPHotlineExclusion(String ivrVIPHotlineExclusion) {
        this.ivrVIPHotlineExclusion = ivrVIPHotlineExclusion;
    }

    public Long getCallerNumber() {
        return callerNumber;
    }

    public void setCallerNumber(Long callerNumber) {
        this.callerNumber = callerNumber;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public String getAssetSubStatus() {
        return assetSubStatus;
    }

    public void setAssetSubStatus(String assetSubStatus) {
        this.assetSubStatus = assetSubStatus;
    }

    public String getFirstCallActivationDate() {
        return firstCallActivationDate;
    }

    public void setFirstCallActivationDate(String firstCallActivationDate) {
        this.firstCallActivationDate = firstCallActivationDate;
    }

    public String getStarterPackBrand() {
        return starterPackBrand;
    }

    public void setStarterPackBrand(String starterPackBrand) {
        this.starterPackBrand = starterPackBrand;
    }

    public String getStarterPackName() {
        return starterPackName;
    }

    public void setStarterPackName(String starterPackName) {
        this.starterPackName = starterPackName;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCcToken() {
        return ccToken;
    }

    public void setCcToken(String ccToken) {
        this.ccToken = ccToken;
    }

    public String getEliteIndicator() {
        return eliteIndicator;
    }

    public void setEliteIndicator(String eliteIndicator) {
        this.eliteIndicator = eliteIndicator;
    }

    public String getServiceBundleClass() {
        return serviceBundleClass;
    }

    public void setServiceBundleClass(String serviceBundleClass) {
        this.serviceBundleClass = serviceBundleClass;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "crmServiceID='" + crmServiceID + '\'' +
                ", ivrLanguage='" + ivrLanguage + '\'' +
                ", serviceType='" + serviceType + '\'' +
                ", product='" + product + '\'' +
                ", crmCustomerID='" + crmCustomerID + '\'' +
                ", customerName='" + customerName + '\'' +
                ", accountCategory='" + accountCategory + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", nationality='" + nationality + '\'' +
                ", race='" + race + '\'' +
                ", gender='" + gender + '\'' +
                ", customerIDNo='" + customerIDNo + '\'' +
                ", customerIDType='" + customerIDType + '\'' +
                ", vipCode='" + vipCode + '\'' +
                ", segmentGroup='" + segmentGroup + '\'' +
                ", segmentCode='" + segmentCode + '\'' +
                ", segmentSubGroup='" + segmentSubGroup + '\'' +
                ", buildingName='" + buildingName + '\'' +
                ", floorNumber='" + floorNumber + '\'' +
                ", streetType='" + streetType + '\'' +
                ", streetAddress='" + streetAddress + '\'' +
                ", houseUnitLotNumber='" + houseUnitLotNumber + '\'' +
                ", postcode='" + postcode + '\'' +
                ", poBox='" + poBox + '\'' +
                ", state='" + state + '\'' +
                ", city='" + city + '\'' +
                ", addressType='" + addressType + '\'' +
                ", bestCallTime='" + bestCallTime + '\'' +
                ", tPIN='" + tPIN + '\'' +
                ", ivrHotlineExclusion='" + ivrHotlineExclusion + '\'' +
                ", ivrVIPHotlineExclusion='" + ivrVIPHotlineExclusion + '\'' +
                ", callerNumber=" + callerNumber +
                ", assetStatus='" + assetStatus + '\'' +
                ", assetSubStatus='" + assetSubStatus + '\'' +
                ", firstCallActivationDate='" + firstCallActivationDate + '\'' +
                ", starterPackBrand='" + starterPackBrand + '\'' +
                ", starterPackName='" + starterPackName + '\'' +
                ", plan='" + plan + '\'' +
                ", registrationDate='" + registrationDate + '\'' +
                ", ccToken='" + ccToken + '\'' +
                ", eliteIndicator='" + eliteIndicator + '\'' +
                ", serviceBundleClass='" + serviceBundleClass + '\'' +
                ", creditLimit='" + creditLimit + '\'' +
                ", errorCode='" + errorCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
